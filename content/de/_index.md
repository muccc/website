---
title: "Willkommen im Münchner Chaos Computer Club"
date: 2024-07-13T00:00:00Z
draft: false
---

## Willkommen im Münchner Chaos Computer Club

Als Hackspace setzen wir uns kritisch und kreativ mit Technik auseinander. Das heißt wir benutzen Technik nicht nur, sondern schrauben sie auf (das betrifft Hardware wie Software), versuchen zu verstehen, wie sie funktioniert und verwenden sie auch mal zu völlig anderen Zwecken als ursprünglich gedacht. Dabei sind für uns auch ethische Aspekte wichtig, z.B. Datenschutz, Softwaresicherheit oder der Protest gegen den Überwachungskapitalismus. Auch künstlerisch-kreativer Umgang mit Technik kommt bei uns nicht zu kurz.

Klingt spannend? Dann komm vorbei, wir freuen uns auf dich! 🙂

<br clear="all">

## Das Chaos kennenlernen: Public Tuesday

Wenn du uns und unsere Räume kennenlernen möchtest, komm gerne zu unserem Public Tuesday, immer am *2. Dienstag* im Monat . Beginn ist um *20:00 Uhr*, wenn du Lust hast kannst du dich auch ab 18:30 unserer Haküfa anschließen, wir kochen zusammen ein veganes Abendessen im Club. Gegen 21 Uhr gibt es eine Führung durch die Räume. Falls du Fragen zum Treffen hast, schau doch mal in unsere FAQs.


## Veranstaltungen und Gruppen

Viele unserer Veranstaltungen sind für Nicht-Mitglieder offen ("public") - im [Veranstaltungskalender](/de/calendar) findest du den Überblick über anstehende Termine. Details zu den verschiedenen Gruppen (z.B. Elektronikfreunde, Rust, CTF, F.U.C.K., Lockpickung u.v.m.) findest du im Wiki.

<br clear="all">

<div id="map"><noscript><img src="/images/location-map.jpg" alt="Karte die den muCCC anzeigt in der Schleißheimerstr.39, München" /></noscript></div>

<br clear="all">


## All creatures welcome

Egal wie viel oder wenig Erfahrung ihr habt welches Alter, ob  Hacker*in, Maker*in oder einfach nur neugierig, ihr seid willkommen! Wenn ihr euch für  Themen wie Coding, Elektronik & Löten, Holzarbeiten, Lasercutting, CNC-Fräsen, 3D-Drucken, Musik, Textilien & Nähen usw. inxteressiert, seid ihr bei uns richtig.

## Der Club

![](/images/space-map.png)

Finde heraus was man bei uns alles tun kann unter

## Barrieren

Die Räume des Münchner CCCs sind leider nicht barrierefrei. Auf der Seite Barrierefreiheit findest du Informationen zum Layout und Einschränkungen der Räume, damit du einschätzen kannst, ob der Zugang möglich ist oder nicht.

Falls du Fragen hast, oder du gerne kommen würdest aber dabei Hilfe bräuchtest, schreibe und im IRC oder per Email an. Wir ergänzen diese Seite gerne und helfen dir auch - beim Zugang in unsere Räume.
