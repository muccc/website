{
  description = "MuCCC Website";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    systems.url = "github:nix-systems/default";
  };

  outputs =
    inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;

      perSystem =
        { pkgs, ... }:
        {
          devShells.default = pkgs.mkShellNoCC { packages = [ pkgs.hugo ]; };

          apps.default.program = pkgs.writers.writeBashBin "hugo-serve" { } ''
            ${pkgs.hugo}/bin/hugo serve --disableFastRender
          '';

          packages.default = pkgs.stdenv.mkDerivation {
            name = "muccc-website";
            src = inputs.self;

            nativeBuildInputs = [ pkgs.hugo ];

            dontConfigure = true;
            dontInstall = true;

            buildPhase = ''
              hugo -d "$out"
            '';
          };
        };
    };
}
